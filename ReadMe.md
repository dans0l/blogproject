# Building a Blogging Application using Python in Django

## Quick Guide
Below are the steps on how to get the web app up and running

1.	Clone it:
```bash
    git clone https://bitbucket.org/dans0l/blogProject.git
```
2.	Cd into it:
```bash
    cd blogProject
```
3.	Create a venv
```bash
    python3 -m venv venv
```
4.	Activate venv:
```bash
    Mac/Linux: source venv/bin/activate
    Windows: venv\Scripts\activate
```
5.	Install the requirements
```bash
    pip install -r requirements.txt
```
6.	Create DB
```bash
    python manage.py makemigrations
```
7.	Apply DB Changes
```bash
    python manage.py migrate
```
8.	Run the server:
    - python manage.py runserver
9.	Navigate to your [localhost](http://127.0.0.1:8000) site
10.	Follow the instructions on the about page to start using the site
